package noe.CAMTest.SQL;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class CAMDatabase extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "student.db";
    public static final int DATABASE_VERSION = 1;

    public static final String CREATE_TABLE_AREAS = "create table " + ContractClass.AreaTable.TABLE_NAME +
    " (" + ContractClass.AreaTable.ID + " number, " + ContractClass.AreaTable.NOMBRE +  " text, "+ ContractClass.AreaTable.IMAGEN + " text);";

    public static final String CREATE_TABLE_ESPECIALIDADES = "create table " + ContractClass.EspecialidadTable.TABLE_NAME +
    " (" + ContractClass.EspecialidadTable.ID + " number, " + ContractClass.EspecialidadTable.ID_AREA +  " text, " + ContractClass.EspecialidadTable.NOMBRE +  " text);";

    public static final String CREATE_TABLE_TEMAS = "create table " + ContractClass.TemaTable.TABLE_NAME +
    " (" + ContractClass.TemaTable.ID + " number, " + ContractClass.TemaTable.ID_ESPECIALIDAD +  " text, " + ContractClass.TemaTable.NOMBRE +  " text, " + ContractClass.TemaTable.EDITABLE +  " text);";

    public static final String DROP_TABLE_AREAS = "drop table if exists " + ContractClass.AreaTable.TABLE_NAME;
    public static final String DROP_TABLE_ESPECIALIDADES = "drop table if exists " + ContractClass.EspecialidadTable.TABLE_NAME;
    public static final String DROP_TABLE_TEMAS = "drop table if exists " + ContractClass.TemaTable.TABLE_NAME;


    public CAMDatabase(Context context) {
        super(context, DATABASE_NAME, null,DATABASE_VERSION);
        Log.d("DB Operations", "DB Created");
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            db.execSQL(CREATE_TABLE_AREAS);
            db.execSQL(CREATE_TABLE_ESPECIALIDADES);
            db.execSQL(CREATE_TABLE_TEMAS);
        }catch (SQLiteException sqle){
            onUpgrade(db,DATABASE_VERSION,DATABASE_VERSION);
        }
        Log.d("DB Operations", "Table Created");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DROP_TABLE_AREAS);
        db.execSQL(DROP_TABLE_ESPECIALIDADES);
        db.execSQL(DROP_TABLE_TEMAS);
        onCreate(db);
    }

    public void addArea(int id, String nombre, String imagenURL, SQLiteDatabase database){
        ContentValues contentValues = new ContentValues();
        contentValues.put(ContractClass.AreaTable.ID, id);
        contentValues.put(ContractClass.AreaTable.NOMBRE, nombre);
        contentValues.put(ContractClass.AreaTable.IMAGEN, imagenURL);
        database.insert(ContractClass.AreaTable.TABLE_NAME, null, contentValues);
        Log.d("DB Operations", "Area row inserted");
    }

    public void addEspecialidad(int id, int idArea, String nombre, SQLiteDatabase database){
        ContentValues contentValues = new ContentValues();
        contentValues.put(ContractClass.EspecialidadTable.ID, id);
        contentValues.put(ContractClass.EspecialidadTable.ID_AREA, idArea);
        contentValues.put(ContractClass.EspecialidadTable.NOMBRE, nombre);
        database.insert(ContractClass.EspecialidadTable.TABLE_NAME, null, contentValues);
        Log.d("DB Operations", "Especialidad row inserted");
    }

    public void addTema(int id, int especialidadID, String nombre, String editable, SQLiteDatabase database)
    {
        ContentValues contentValues = new ContentValues();
        contentValues.put(ContractClass.TemaTable.ID, id);
        contentValues.put(ContractClass.TemaTable.ID_ESPECIALIDAD, especialidadID);
        contentValues.put(ContractClass.TemaTable.NOMBRE, nombre);
        contentValues.put(ContractClass.TemaTable.EDITABLE, editable);
        database.insert(ContractClass.TemaTable.TABLE_NAME, null, contentValues);
        Log.d("DB Operations", "Tema row inserted");
    }

    public Cursor readAreas(SQLiteDatabase database){
        String[] projections = {ContractClass.AreaTable.ID, ContractClass.AreaTable.NOMBRE, ContractClass.AreaTable.IMAGEN};

        Cursor cursor = database.query(
                ContractClass.AreaTable.TABLE_NAME,
                projections,
                null,
                null,
                null,
                null,
                null
        );
        return cursor;
    }

    public Cursor readEspecialidadesForArea(SQLiteDatabase database,String areaID)
    {
        String[] projections = {ContractClass.EspecialidadTable.ID, ContractClass.EspecialidadTable.NOMBRE};
        String whereClause = ContractClass.EspecialidadTable.ID_AREA + " = ?";
        String whereArgs[] = new String[]{areaID};

        Cursor cursor = database.query(
        ContractClass.EspecialidadTable.TABLE_NAME,
        projections,
        whereClause,
        whereArgs,
        null,
        null,
        null
        );

        return cursor;
    }

    public Cursor readTemasForArea(SQLiteDatabase database, String especialidadID)
    {
       String[] projection = {ContractClass.TemaTable.ID, ContractClass.TemaTable.NOMBRE, ContractClass.TemaTable.EDITABLE};
       String whereClause = ContractClass.TemaTable.ID_ESPECIALIDAD + " = ?";
       String whereArgs[] = new String[]{especialidadID};

       Cursor cursor = database.query(
               ContractClass.TemaTable.TABLE_NAME,
               projection,
               whereClause,
               whereArgs,
               null,
               null,
               null
       );
       return cursor;
    }
}
