package noe.CAMTest.SQL;

public final class ContractClass {

    private ContractClass(){}

    public static final int USUARIO_ID = 23;

    public static class AreaTable
    {
        public static final String TABLE_NAME = "areas";
        public static final String ID = "id";
        public static final String NOMBRE = "nombre";
        public static final String IMAGEN = "imagen";
    }

    public static class EspecialidadTable
    {
        public static final String TABLE_NAME = "especialidades";
        public static final String ID = "id";
        public static final String ID_AREA = "id_area";
        public static final String NOMBRE = "nombre";
    }

    public static class TemaTable
    {
        public static final String TABLE_NAME = "temas";
        public static final String ID = "id";
        public static final String ID_ESPECIALIDAD = "id_especialidad";
        public static final String NOMBRE = "nombre";
        public static final String EDITABLE = "editable";
        public static final String CLAVE_ES_EDTIABLE = "0";
    }

}
