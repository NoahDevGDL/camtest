package noe.CAMTest.DataStructures;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import noe.CAMTest.Interfaces.LoadActivityPassingString;
import noe.CAMTest.R;

public class CAMEspecialidadAdapter extends BaseAdapter implements View.OnClickListener{

    Context context;
    int[] ids;
    String[] names;
    LoadActivityPassingString lapsInterface;

    public CAMEspecialidadAdapter(Context context, int[] ids, String[] names){
        this.context = context;
        this.ids = ids;
        this.names = names;
        this.lapsInterface = (LoadActivityPassingString) context;
    }

    @Override
    public int getCount() {
        return names.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View espItem, ViewGroup parent) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        espItem = layoutInflater.inflate(R.layout.especialidad_row_item,null);
        espItem.setOnClickListener(this);
        espItem.setTag(ids[position]);
        TextView textView = espItem.findViewById(R.id.textViewEspItem);
        textView.setText(names[position]);
        return espItem;
    }

    @Override
    public void onClick(View v) {
        lapsInterface.loadActivity(Integer.toString((int)v.getTag()));
    }
}
