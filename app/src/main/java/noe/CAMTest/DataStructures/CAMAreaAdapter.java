package noe.CAMTest.DataStructures;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import noe.CAMTest.Interfaces.LoadActivityPassingString;
import noe.CAMTest.R;

public class CAMAreaAdapter extends BaseAdapter implements View.OnClickListener{

    Context context;
    int[] ids;
    String[] names;
    String[] imageURLs;
    LoadActivityPassingString loadActivityPassingString;

    public CAMAreaAdapter(Context context, int[] ids,String[] names, String[] imageURLs){
        this.context = context;
        this.ids = ids;
        this.names = names;
        this.imageURLs = imageURLs;
        loadActivityPassingString = (LoadActivityPassingString) context;
    }

    @Override
    public int getCount() {
        return names.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View areaItem, ViewGroup parent) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        areaItem = layoutInflater.inflate(R.layout.area_row_item,null);
        areaItem.setTag(ids[position]);
        areaItem.setOnClickListener(this);
        ImageView imageView = areaItem.findViewById(R.id.imageViewAreaItem);
        TextView textView = areaItem.findViewById(R.id.textViewAreaItem);
        loadImageFromURL(imageView, imageURLs[position]);
        textView.setText(names[position]);
        return areaItem;
    }

    public void loadImageFromURL(ImageView imageView, String url){
        Picasso.with(context).load(url)
        .placeholder(R.mipmap.ic_launcher)
        .error(R.mipmap.ic_launcher_round)
        .into(imageView, new com.squareup.picasso.Callback(){
            @Override
            public void onSuccess() {}
            @Override
            public void onError() {}
        });
    }

    @Override
    public void onClick(View v) {
        String id = Integer.toString((int)v.getTag());
        loadActivityPassingString.loadActivity(id);
    }
}
