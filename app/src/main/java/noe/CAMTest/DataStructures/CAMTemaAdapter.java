package noe.CAMTest.DataStructures;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import noe.CAMTest.R;
import noe.CAMTest.SQL.ContractClass;

public class CAMTemaAdapter extends BaseAdapter{

    Context context;
    int[] ids;
    String[] names, editables;

    public CAMTemaAdapter(Context context, int[] ids, String[] names, String editables[]){
        this.context = context;
        this.ids = ids;
        this.names = names;
        this.editables = editables;
    }

    @Override
    public int getCount() {
        return names.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View temaItem, ViewGroup parent) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        temaItem = layoutInflater.inflate(R.layout.tema_row_item,null);
        TextView textView = temaItem.findViewById(R.id.textViewTemaItem);
        ImageView imageViewEsEditable = temaItem.findViewById(R.id.imageViewEsEditable);
        if(editables[position].equals(ContractClass.TemaTable.CLAVE_ES_EDTIABLE))
            imageViewEsEditable.setImageResource(R.drawable.editable);
        else
            imageViewEsEditable.setImageResource(R.drawable.no_editable);
        textView.setText(names[position]);

        return temaItem;
    }

}
