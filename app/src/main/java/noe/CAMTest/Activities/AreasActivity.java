package noe.CAMTest.Activities;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import noe.CAMTest.DataStructures.CAMAreaAdapter;
import noe.CAMTest.Interfaces.LoadActivityPassingString;
import noe.CAMTest.Networking.HTTPRequestor;
import noe.CAMTest.SQL.CAMDatabase;
import noe.CAMTest.R;
import noe.CAMTest.SQL.ContractClass;

public class AreasActivity extends AppCompatActivity implements LoadActivityPassingString {

    CAMDatabase myDB;
    String[] names, imageURLs;
    int[] ids;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        myDB = new CAMDatabase(this);
        myDB.onCreate(myDB.getWritableDatabase());
        new NetworkThread().execute();
    }

    public void writeToAreasTable(int id, String nombre, String imagenURL){
        SQLiteDatabase db = myDB.getWritableDatabase();
        myDB.addArea(id,nombre,imagenURL, db);
    }

    public void writeToEspecialidadesTable(int id, int idArea, String nombre){
        SQLiteDatabase db = myDB.getWritableDatabase();
        myDB.addEspecialidad(id, idArea, nombre, db);
    }

    public void writeToTemaTable(int id, int especialidadID, String nombre, String editable){
        SQLiteDatabase db = myDB.getWritableDatabase();
        myDB.addTema(id, especialidadID, nombre, editable, db);
    }


    public void readAreasToArrays(){
        SQLiteDatabase db = myDB.getReadableDatabase();
        final Cursor cursor = myDB.readAreas(db);
        int count = cursor.getCount();
        names = new String[count];
        imageURLs = new String[count];
        ids = new int[count];
        int index = 0;
        while(cursor.moveToNext()){
            ids[index] = cursor.getInt(cursor.getColumnIndex(ContractClass.AreaTable.ID));
            names[index] = cursor.getString(cursor.getColumnIndex(ContractClass.AreaTable.NOMBRE));
            imageURLs[index++] = cursor.getString(cursor.getColumnIndex(ContractClass.AreaTable.IMAGEN));
        }
    }

    @Override
    public void loadActivity(String viewID) {
        Intent intent = new Intent(this, EspecialidadesActivity.class);
        intent.putExtra(ContractClass.EspecialidadTable.ID_AREA, viewID);
        startActivity(intent);
    }

    class NetworkThread extends AsyncTask {

        @Override
        protected String doInBackground(Object[] objects) {
            try
            {
                JSONObject jsonObjectParent = new JSONObject(HTTPRequestor.request("usuario_id="+ContractClass.USUARIO_ID));
                String msg = jsonObjectParent.getString(HTTPRequestor.MSG_ATTRIBUTE);

                if (msg.equals(HTTPRequestor.OK_MSG))
                {
                    JSONObject jsonObjectData = jsonObjectParent.getJSONObject("data");
                    //Escribir areas a BD
                    JSONArray jsonArrayAreas = jsonObjectData.getJSONArray(ContractClass.AreaTable.TABLE_NAME);
                    for (int areasIndex = 0; areasIndex < jsonArrayAreas.length(); areasIndex++)
                    {
                        JSONObject aux = jsonArrayAreas.getJSONObject(areasIndex);
                        int idArea = aux.getInt(ContractClass.AreaTable.ID);
                        String nombreArea = aux.getString(ContractClass.AreaTable.NOMBRE);
                        String imagenArea = aux.getString(ContractClass.AreaTable.IMAGEN);
                        writeToAreasTable(idArea, nombreArea, imagenArea);
                        //Escribir especialidades a BD
                        JSONArray jsonArrayEspecialidades = aux.getJSONArray("especialidades");
                        for (int indexEspecialidades = 0; indexEspecialidades < jsonArrayEspecialidades.length(); indexEspecialidades++)
                        {
                            JSONObject auxEsp = jsonArrayEspecialidades.getJSONObject(indexEspecialidades);
                            int idEsp = auxEsp.getInt(ContractClass.EspecialidadTable.ID);
                            String nombreEsp = auxEsp.getString(ContractClass.EspecialidadTable.NOMBRE);
                            writeToEspecialidadesTable(idEsp,idArea,nombreEsp);
                            //Escribir temas a BD
                            JSONArray jsonArrayTemas = auxEsp.getJSONArray(ContractClass.TemaTable.TABLE_NAME);
                            for (int indexTemas = 0; indexTemas < jsonArrayTemas.length(); indexTemas++)
                            {
                                JSONObject auxTema = jsonArrayTemas.getJSONObject(indexTemas);
                                int idTema = auxTema.getInt(ContractClass.TemaTable.ID);
                                String nombreTema = auxTema.getString(ContractClass.TemaTable.NOMBRE);
                                String editable = auxTema.getString(ContractClass.TemaTable.EDITABLE);
                                writeToTemaTable(idTema, idEsp, nombreTema, editable);
                            }
                        }
                    }
                }
                readAreasToArrays();
                return msg;
            }
            catch (JSONException e)
            {
                return e.getMessage();
            }
        }

        @Override
        protected void onPostExecute(Object o) {
            String msg = (String) o;
            if(!msg.equals(HTTPRequestor.OK_MSG))
                Toast.makeText(AreasActivity.this, msg, Toast.LENGTH_LONG).show();

            ListView listView = findViewById(R.id.listView);
            listView.setAdapter(new CAMAreaAdapter(AreasActivity.this, ids, names, imageURLs));
        }
    }
}
