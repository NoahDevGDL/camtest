package noe.CAMTest.Activities;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import noe.CAMTest.DataStructures.CAMEspecialidadAdapter;
import noe.CAMTest.Interfaces.LoadActivityPassingString;
import noe.CAMTest.R;
import noe.CAMTest.SQL.CAMDatabase;
import noe.CAMTest.SQL.ContractClass;

public class EspecialidadesActivity extends AppCompatActivity implements LoadActivityPassingString{

    private int[] ids;
    private String[] names;
    CAMDatabase camDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_especialidades);
        camDatabase = new CAMDatabase(this);
        Bundle bundle = getIntent().getExtras();
        String areaID = bundle.getString(ContractClass.EspecialidadTable.ID_AREA);
        loadEspecialidadesForArea(Integer.parseInt(areaID));
        ListView listView = findViewById(R.id.listViewEspecialiades);
        listView.setAdapter(new CAMEspecialidadAdapter(this, ids, names));
    }

    private void loadEspecialidadesForArea(int areaID)
    {
        SQLiteDatabase database = camDatabase.getReadableDatabase();
        Cursor cursor = camDatabase.readEspecialidadesForArea(database, Integer.toString(areaID));
        int count = cursor.getCount();
        ids = new int[count];
        names = new String[count];
        int indexEsp = 0;

        while (cursor.moveToNext()){
            ids[indexEsp] = cursor.getInt(cursor.getColumnIndex(ContractClass.EspecialidadTable.ID));
            names[indexEsp++] = cursor.getString(cursor.getColumnIndex(ContractClass.EspecialidadTable.NOMBRE));
        }
    }

    @Override
    public void loadActivity(String viewID) {
        Intent intent = new Intent(this, TemasActivity.class);
        intent.putExtra(ContractClass.TemaTable.ID_ESPECIALIDAD, viewID);
        startActivity(intent);
    }
}
