package noe.CAMTest.Activities;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import noe.CAMTest.DataStructures.CAMTemaAdapter;
import noe.CAMTest.R;
import noe.CAMTest.SQL.CAMDatabase;
import noe.CAMTest.SQL.ContractClass;

public class TemasActivity extends AppCompatActivity {

    CAMDatabase camDatabase;
    String[] names, editables;
    int[] ids;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temas);
        camDatabase = new CAMDatabase(this);
        Bundle bundle = getIntent().getExtras();
        String idEsp = bundle.getString(ContractClass.TemaTable.ID_ESPECIALIDAD);
        loadTemasForEspecialidad(idEsp);
        ListView listViewTemas = findViewById(R.id.listViewTemas);
        listViewTemas.setAdapter(new CAMTemaAdapter(this, ids, names, editables));
    }

    private void loadTemasForEspecialidad(String areaID) {
        SQLiteDatabase sqLiteDatabase = camDatabase.getReadableDatabase();
        Cursor cursor = camDatabase.readTemasForArea(sqLiteDatabase, areaID);
        int count = cursor.getCount();
        names = new String[count];
        editables = new String[count];
        ids = new int[count];
        int temaIndex = 0;

        while (cursor.moveToNext()){
            ids[temaIndex] = cursor.getInt(cursor.getColumnIndex(ContractClass.TemaTable.ID));
            names[temaIndex] = cursor.getString(cursor.getColumnIndex(ContractClass.TemaTable.NOMBRE));
            editables[temaIndex++] = cursor.getString(cursor.getColumnIndex(ContractClass.TemaTable.EDITABLE));
        }
    }
}
