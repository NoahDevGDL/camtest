package noe.CAMTest.Networking;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import javax.net.ssl.HttpsURLConnection;

public class HTTPRequestor {

	private static final int HTTPS_URL_READ_TIMEOUT = 15000;
    private static final int HTTPS_URL_CONNECT_TIMEOUT = 15000;
    private static final String MAIN_DOMAIN = "http://oomovil.x10.mx/cam_nuevo/";
    private static final String API_URL = "index.php/api/listado_areas_espe_temas";
    public static final String HTTP_ERROR_RESPONSE_JSON = "{'msg':'Error de conexion HTTP!'}";
    public static final String EXCEPTION_RESPONSE_JSON = "{'msg':'Error de conexion!'}";
    public static final String MSG_ATTRIBUTE = "msg";
    public static final String OK_MSG = "ok";

    public static String request(String id)
    {
        String response = "";
        HttpURLConnection httpURLConnection = null;
        try
        {
            httpURLConnection = prepareHTTPURLConnection(MAIN_DOMAIN + API_URL);
            httpURLConnection.setFixedLengthStreamingMode(id.getBytes().length);
            PrintWriter writer = new PrintWriter(httpURLConnection.getOutputStream());
            writer.print(id);
            writer.close();

            int responseCode = httpURLConnection.getResponseCode();
            if (responseCode == HttpsURLConnection.HTTP_OK)
            {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
                while ((line=br.readLine()) != null) {
                    response+=line;
                }
            }
            else
            {
                response = HTTP_ERROR_RESPONSE_JSON;
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            response = EXCEPTION_RESPONSE_JSON;
        }
        finally
        {
            if(httpURLConnection != null) httpURLConnection.disconnect();
        }
        return response;
    }

    private static HttpURLConnection prepareHTTPURLConnection(String urlString){
        HttpURLConnection httpURLConnectionAux = null;
        try {
            URL urlAux = new URL(urlString);
            httpURLConnectionAux = (HttpURLConnection) urlAux.openConnection();
            httpURLConnectionAux.setReadTimeout(HTTPS_URL_READ_TIMEOUT);
            httpURLConnectionAux.setConnectTimeout(HTTPS_URL_CONNECT_TIMEOUT);
            httpURLConnectionAux.setRequestMethod("POST");
            httpURLConnectionAux.setDoInput(true);
            httpURLConnectionAux.setDoOutput(true);
            httpURLConnectionAux.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return httpURLConnectionAux;
    }
}
